import pyjapc
import matplotlib.pyplot as plt
from cern_general_devices import bgi
from cern_general_devices import bsg
from cern_general_devices import bpm
import os
import datetime
from datetime import timedelta
import pytz
import time
import json
import glob
import logging
from pybt.myjson.encoder import myJSONEncoder
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt

import pandas as pd
import os
import numpy as np
import warnings

import functions as fs

verbose = False

userName = "CPS.USER.EAST4"
userNamePSB = "PSB.USER.MD3"
myMonitor = pyjapc.PyJapc(userName)

bct_threshold = -1

allOK = False
last_bgi_file = []

# Create a folder to save data
root_folder, folder_name, time_title = fs.create_folder("bpm_data","bpm")

bct_names = ["BTP.BCT10/Acquisition#totalIntensityCalcCh1"]

bpm_properties = ['position','positionH','positionV', 'channelNamesH','channelNames','channelNamesV','channelNames']
bpmPS = bpm.BPMOPS(myMonitor, 'PR.BPM', timingSelectorOverride=userName)


# Machine state
machine_state_name_list = ["PE.SMH57/MEAS.PULSE#VALUE", #CPS:EJ61
                       "PE.SMH57/REF.FUNC.PLAY#value",
                        "logical.PE.SMH57/I_FUNC_LIST#value",
                        "logical.PE.SMH57/KL_FUNC_LIST#value",
                       "PE.SMH61/MEAS.PULSE#VALUE",
                       "PE.SMH61/REF.FUNC.PLAY#value",
                        "logical.PE.SMH61/I_FUNC_LIST#value",
                        "logical.PE.SMH61/KL_FUNC_LIST#value",
                       "PE.BSW23/REF.TABLE.FUNC#VALUE",
                       "PE.BSW23/REF.FUNC.PLAY#value",
                       "PE.BSW23/MEAS.PULSE#VALUE",
                        "logical.PE.BSW23/I",
                        "logical.PE.BSW23/K",
                       "PE.BSW57/REF.TABLE.FUNC#VALUE",
                       "PE.BSW57/REF.FUNC.PLAY#value",
                       "PE.BSW57/MEAS.PULSE#VALUE",
                       "logical.PE.BSW57/I#value",
                        "logical.PE.BSW57/KL#value",
                       "PR.QSE/REF.TABLE.FUNC#VALUE",
                       "PR.QSE/REF.FUNC.PLAY#value",
                       "PR.QSE/MEAS.PULSE#VALUE",
                       "PR.XSE/REF.TABLE.FUNC#VALUE",
                       "PR.XSE/REF.FUNC.PLAY#value",
                       "PR.XSE/MEAS.PULSE#VALUE",
                        "PE.KFA71-V/Acquisition", #CPS:KICKERS
                        "PE.KFA71-V/Setting",
                        "PE2.KFA71-V/Acquisition",
                        "PE2.KFA71-V/Setting",
                        "logical.PE.KFA71/I",
                        "logical.PE.KFA71/K",
                        'PE.KFA4/BatchAcquisition#kickStrengthAcq',
                        "PR.DHZ05.OC/MEAS.PULSE#VALUE", #CPS:MULTIPOLES-HE
                        "PR.DHZ05.OC/REF.TABLE.FUNC#VALUE",
                       "PR.DHZ05.OC/REF.FUNC.PLAY#value",
                        "PR.DHZ18.OC/MEAS.PULSE#VALUE",
                        "PR.DHZ18.OC/REF.TABLE.FUNC#VALUE",
                       "PR.DHZ18.OC/REF.FUNC.PLAY#value",
                        "PR.DHZ60.OC/MEAS.PULSE#VALUE",
                       "PR.DHZ60.OC/REF.FUNC.PLAY#value",
                        "PR.DHZ60.OC/REF.TABLE.FUNC#VALUE",
                        "logical.PR.DHZ05-EJ/I",
                        "logical.PR.DHZ05-EJ/K",
                        "logical.PR.DHZ05-FT/I",
                        "logical.PR.DHZ05-FT/K",
                       "logical.PR.DHZ05-TR/I",
                       "logical.PR.DHZ05-TR/K",
                       "logical.PR.DHZ18-EJ/I",
                       "logical.PR.DHZ18-EJ/K",
                       "logical.PR.DHZ18-FT/I",
                       "logical.PR.DHZ18-FT/K",
                       "logical.PR.DHZ18-TR/I",
                       "logical.PR.DHZ18-TR/K",
                       "logical.PR.DHZ60-EJ/I",
                       "logical.PR.DHZ60-EJ/K",
                       "logical.PR.DHZ60-FT/I",
                       "logical.PR.DHZ60-FT/K",
                       "logical.PR.DHZ60-TR/I",
                       "logical.PR.DHZ60-TR/K",
                        "logical.PR.QSE/I",
                        "logical.PR.QSE/K",
                       "logical.PR.XSE/I",
                       "logical.PR.XSE/K",
                       ]
machine_state_dict = {machine_state_name: [] for machine_state_name in machine_state_name_list}

# no selector device

machine_state_no_sel_name_list = ["PE.SEH23/Acquisition"]

machine_state_no_sel_dict = {machine_state_no_sel_name: [] for machine_state_no_sel_name in machine_state_no_sel_name_list}

bct = {}
bct_list = []
btp_quad = {}
freq_psb = {}
bpmPS_data = {}
bpmPS_data_list = []

def callBack(paramName, newValue, headerData):
    global allOK
    checkFirstUpdate = headerData['isFirstUpdate']
    if not checkFirstUpdate:
        for i in bct_names:
            bct[i] = myMonitor.getParam(i, timingSelectorOverride=userNamePSB)
            checkBCT = bct["BTP.BCT10/Acquisition#totalIntensityCalcCh1"]
        if np.min(checkBCT) > bct_threshold:
            if not allOK:
                print('New cycle acquired!')
                print('Cycle Stamp = ' + str(headerData['cycleStamp']))
                #print(paramName, newValue)
                allOK = True
                print('Check flag', str(allOK))
            else:
                print('Cycle blocked waiting for another!')
        else:
            logging.warning("Shot ignored on BCT ring intensity")
    else:
        print('Skipping first update!')


myMonitor.subscribeParam('XTIM.PX.ECY-CT/Acquisition#acqC',callBack, getHeader=True,timingSelectorOverride=userName)
myMonitor.startSubscriptions()

print('Wait to ignore first acquisitions...')


while True:
    if allOK == True:

        print('Grabbing data PYJAPC...')

        bct_list.append(bct)

        bpmPS_data_temp, bpmPS_data['header'] = bpmPS.getTrajectoryBBB()
        # bpmPS_data_temp, bpmPS_data['header'] = bpmPS.getOrbit()

        for i in bpm_properties:
            bpmPS_data[i] = bpmPS_data_temp[i]

        bpmPS_data_list.append(bpmPS_data)

        # Append all new parameters for the new shot
        for machine_state_name in machine_state_name_list:
            parameter = myMonitor.getParam(machine_state_name, timingSelectorOverride=userName)
            machine_state_dict[machine_state_name].append(parameter)

        for machine_state_no_sel_name in machine_state_no_sel_name_list:
            parameter = myMonitor.getParam(machine_state_no_sel_name, timingSelectorOverride="") # Remove selector
            machine_state_no_sel_dict[machine_state_no_sel_name].append(parameter)

        # JSON data dump
        with open(root_folder + "/" + folder_name + "/" + folder_name + ".json", 'w') as outfile:
            outfile.write(json.dumps((bct_list, bpmPS_data_list, machine_state_dict, machine_state_no_sel_dict), cls=myJSONEncoder))

        print('Data saved...')

        #bpmPS.plotTrajectoryEvolution()

        allOK = False

        print('Waiting for new cycle')
