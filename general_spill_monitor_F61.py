import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import pandas as pd
import datetime
import functions as fs
import pickle

time_at_script_start = datetime.datetime.now()

# Create a folder to save data
root_folder, folder_name, time_title = fs.create_folder("spill_data_xsec",
                                                        "spill_data_xsec")

japc = pyjapc.PyJapc(noSet=True)
japc.setSelector("CPS.USER.EAST3")

fig, ax = plt.subplots(2,1, tight_layout=True)
fig.canvas.manager.set_window_title(f'xsec monitor started at: {time_at_script_start.strftime("%Y %m %d %Hh%Mm%Ss")}')
fig.suptitle(f'xsec monitor {time_at_script_start.strftime("%Y %m %d %Hh%Mm%Ss")}')

# First data for the heatmap
signal = -japc.getParam("F61.XSEC023-I1/SpillData#semSpillData") # Flip the sign of the signal to have positive spill
integrationDuration = japc.getParam("F61.XSEC023-I1/Acquisition#integrationDuration")
nbOfSamples = japc.getParam("F61.XSEC023-I1/Acquisition#nbOfSamples")

heatmap = np.reshape(signal, (1, len(signal)))
time_list = [time_at_script_start.strftime("%Hh%Mm%Ss")]

# Data we want to save
timestamp_list = []
spill_monitor_name_list = ["F61.XSEC023-I1/SpillData",
                           "F61.XSEC023-I2/SpillData",
                           "F61.XSEC023-I1/Acquisition",
                           "F61.XSEC023-I2/Acquisition",]
spill_monitor_dict = {spill_monitor: [] for spill_monitor in spill_monitor_name_list}

def myCallback(parameterName, newValue):

    global heatmap
    global time_list

    ax[0].clear()
    ax[1].clear()

    # Raw data
    signal_23_I1 = japc.getParam("F61.XSEC023-I1/SpillData#semSpillData")
    signal_23_I2 = japc.getParam("F61.XSEC023-I2/SpillData#semSpillData")



    if (len(signal_23_I1) == nbOfSamples and len(signal_23_I2) == nbOfSamples): # Error, sometimes XSEC publishes 41 data points

        time = datetime.datetime.now()
        time_list.append(time.strftime("%Hh%Mm%Ss"))
        fig.suptitle(time.strftime("Spill waterfall East Area \n Most recent acquisition: %Hh%Mm%Ss"))

        # Append all new data for the new shot
        timestamp_list.append(time)
        for spill_monitor in spill_monitor_name_list:
            data = japc.getParam(spill_monitor)
            spill_monitor_dict[spill_monitor].append(data)

        t = np.arange(0, integrationDuration, int(integrationDuration / nbOfSamples))  # time in ms

        d = {'t': t, 'signal_23_I1': signal_23_I1, 'signal_23_I2': signal_23_I2} # Flip the sign of the signal to have positive spill
        df = pd.DataFrame(data=d)
        #
        ax[0].plot(df.t, -df.signal_23_I1, color="b", alpha=1.0, label="Raw Signal XSEC023-I1")
        ax[0].plot(df.t, -df.signal_23_I2, color="r", alpha=1.0, label="Raw Signal XSEC023-I2")

        # Waterfall heatmap
        try:
            heatmap=np.vstack([-df.signal_23_I1, heatmap])
        except:
            print("error stacking new data into matrix")

        ax[1].imshow(heatmap, cmap="magma", aspect='auto', interpolation="none")

        ax[0].set_xlabel("Time [ms]")
        ax[0].set_ylabel("Amplitude [arb.]")
        ax[0].set_xlim(0,t.max())
        ax[0].legend(loc="upper left")

        x_axis = ax[1].axes.get_xaxis()
        x_axis.set_visible(False)
        step = np.shape(heatmap)[0]//10 + 1
        ax[1].set_yticks(np.arange(0, np.shape(heatmap)[0],  step))
        try:
            ax[1].set_yticklabels(time_list[::-step]) # Reverse order
        except:
            print("error on yticklabels")

        ax[1].set_ylabel("Time")

        fig.canvas.draw() # Update the plot

        try:
            # Pickle data dump
            pickle.dump((timestamp_list, spill_monitor_dict),
                        open(root_folder + "/" + folder_name + "/" + folder_name + '.p', 'wb'))
        except:
            print('Error on saving data json pickle')
        try:
            # Save figure
            fig.savefig(root_folder + "/" + folder_name + "/" + folder_name + ".png", facecolor='white',
                        transparent=False,
                        bbox_inches='tight');
        except:
            print('error on saving image')

    else:
        print(f"XSEC data is not equal to {nbOfSamples}")

japc.subscribeParam("F61.XSEC023-I1/SpillData#semSpillData", myCallback)
japc.startSubscriptions()
fig.show()
