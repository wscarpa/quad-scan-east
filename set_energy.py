import pyjapc
import numpy as np
import functions as fs
import pickle

japc = pyjapc.PyJapc(noSet=False)
japc.setSelector("CPS.USER.MD6")
japc.rbacLogin()

bfield = japc.getParam("PR.MPC/REF.PPPL#REF4")
print(f'Actual B-field = {bfield[0]} [G]')

new_bfield = 2621
bfield[0] = new_bfield
print(f'New B-field = {bfield[0]} [G]')
japc.setParam("PR.MPC/REF.PPPL#REF4", bfield, checkDims=False)

# Change the bending magnet
dump_bend = -66 * new_bfield / 2581
# dump_bend = -34
japc.setParam("F61.BHZ01.DUMP.A/REF.PULSE.AMPLITUDE#VALUE", dump_bend)
japc.setParam("F61.BHZ01.DUMP.B/REF.PULSE.AMPLITUDE#VALUE", dump_bend)
print(f'Setting dump bend current to = {dump_bend} [A]')

import sys
sys.exit()