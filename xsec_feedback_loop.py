import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import datetime
import pybobyqa
import threading
import logging
# logging.basicConfig(level=logging.INFO, format='%(message)s')

japc = pyjapc.PyJapc(noSet=True)
japc.setSelector("CPS.USER.EAST3")

event = threading.Event()
skip_first_shot = True

def myCallback(parameterName, newValue):
    global skip_first_shot

    print("Got a new shot from cycle")
    signal = japc.getParam("F61.XSEC023-I1/SpillData#semSpillData")

    if (len(signal)==42) and (skip_first_shot==False): # Check if the signal has a constant length
        event.set()
    if (len(signal)!=42):
        print("skipped shot because not 42 long")
    if (skip_first_shot):
        print("skipped first shot")
        skip_first_shot = False

objective_list = []

signal_list = []
control_list = []

# Define the objective function
def my_optimisation_function(control):
    global objective_list
    ax[0].clear()
    ax[1].clear()
    ax[2].clear()

    print("In optimisation loop\n Setting new QX_LEQ control")
    ctime = [0, 1000, 1200, 1350, 1434, 1690, 1740, 2400]
    double_zero = np.array([0, 0,])
    my_control = np.concatenate([double_zero, control, double_zero], )
    control_list.append(my_control)
    setting = np.stack([ctime, my_control], axis=0)
    japc.setParam('PSBEAM/QX_LEQ', setting)

    print("waiting for new shot")
    event.wait()
    print("proceeding with optimisation")

    signal = -japc.getParam("F61.XSEC023-I1/SpillData#semSpillData")
    signal_list.append(signal)

    integrationDuration = japc.getParam("F61.XSEC023-I1/Acquisition#integrationDuration")
    nbOfSamples = japc.getParam("F61.XSEC023-I1/Acquisition#nbOfSamples")
    t = np.arange(0, integrationDuration, int(integrationDuration / nbOfSamples))  # time in ms

    number_of_traces_to_keep = 5
    for i in range(number_of_traces_to_keep):
        alpha=i/number_of_traces_to_keep
        try:
            ax[0].plot(ctime, control_list[0], color="r", alpha=0.5, zorder=0)
            ax[0].plot(ctime, control_list[-number_of_traces_to_keep+i], marker="o", ms=2, alpha=alpha, color="b", zorder=i+1)
        except:
            pass
    ax[0].set_xlabel("Time [ms]")
    ax[0].set_title("Control QX_LEQ")

    for i in range(number_of_traces_to_keep):
        alpha=i/number_of_traces_to_keep
        try:
            ax[1].plot(t, signal_list[0], color="r", alpha=0.5, zorder=0)
            ax[1].plot(t, signal_list[-number_of_traces_to_keep+i], marker="o", ms=2, alpha=alpha, color="b", zorder=i+1)
        except:
            pass

    objective = np.concatenate([np.zeros(13), np.ones(22) * 800, np.zeros(7)])  # 42 elements

    ax[1].plot(t, objective, label="objective", color="k", ls="--")
    ax[1].set_title("Signal XSEC23_I1")
    ax[1].set_xlabel("Time [ms]")


    objective_abs = (abs(np.array(signal) - objective)).sum()
    objective_list.append(objective_abs)

    ax[2].plot(objective_list, color="k")
    ax[2].set_title("Objective function")
    ax[2].set_xlabel("Iterations")

    print("")

    plt.pause(0.05)
    event.clear()
    return objective_abs

fig, ax = plt.subplots(1,3, tight_layout=True, figsize=(10,5))
ax[0].set_ylim(0, 0.03)
ax[1].set_ylim(0, 1200)

japc.subscribeParam("F61.XSEC023-I1/SpillData#semSpillData", myCallback)
japc.startSubscriptions()

# control = japc.getParam("PSBEAM/QX_LEQ")
# print(control)

fig.show()

# Define the starting point
control = np.array([0.012846, 0.017848, 0.019486, 0.02235])

# Define bound constraints (lower <= x <= upper)
lower = np.array([0.01, 0.012, 0.015, 0.02])
upper = np.array([0.02, 0.02, 0.025, 0.03])

# Call Py-BOBYQA
soln = pybobyqa.solve(my_optimisation_function, control, bounds=(lower, upper), objfun_has_noise=True, maxfun=100, rhobeg=0.002)

# Display output
print(soln)


