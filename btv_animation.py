import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation
import matplotlib.dates as mdates
import datetime
import os
import functions as fs
import json
from pybt.myjson.encoder import myJSONEncoder
import pickle
import sys
import time

print(f"BTV animation script")

# We start an instance of pyjapc and choose the correct USER
japc = pyjapc.PyJapc(noSet=True)
user = "CPS.USER.EAST3"
japc.setSelector(user)

btv_name_image = "F61D.BTV010/Image"


##################
# Animation part #
##################

btv_data_image = japc.getParam(btv_name_image)
number_of_acqu = len(btv_data_image["imageSet"])


def myCallback(parameterName, newValue, header):
	print(f"New value for {parameterName}")
	global btv_data_image
	global number_of_acqu
	btv_data_image = japc.getParam(btv_name_image)
	number_of_acqu = len(btv_data_image["imageSet"])
	
japc.subscribeParam("F61D.BTV010/Image", myCallback, getHeader=True)
japc.startSubscriptions()

fig, ax = plt.subplots(tight_layout=True)
fig.canvas.manager.set_window_title('Last Shot')
fig.show()


def animate_func(i):
	ax.clear()
	try:
		image = btv_data_image["imageSet"][i]
		reshaped_image = image.reshape(264,-1)
	except:
		image = btv_data_image["imageSet"][0] # If there are not enough frame, just send first frame
		reshaped_image = image.reshape(264,-1)
	ax.imshow(reshaped_image, interpolation='nearest', cmap="plasma")
	ax.set_title(f"Acq: {i}")
	return

anim = animation.FuncAnimation(
			       fig, 
			       animate_func, 
			       frames = 7,
			       interval = 100, # in ms
			       )











