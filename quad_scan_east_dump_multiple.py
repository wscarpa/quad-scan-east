import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.dates as mdates
from matplotlib import cm
import datetime
import functions as fs
import json
from pybt.myjson.encoder import myJSONEncoder
import pickle

time_at_script_start = datetime.datetime.now()


print(f'''
   ____                  _    _____                 
  / __ \                | |  / ____|                
 | |  | |_   _  __ _  __| | | (___   ___ __ _ _ __  
 | |  | | | | |/ _` |/ _` |  \___ \ / __/ _` | '_ \ 
 | |__| | |_| | (_| | (_| |  ____) | (_| (_| | | | |
  \___\_\\__,_|\__,_|\__,_| |_____/ \___\__,_|_| |_|
{time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss")}                                                                                                                                                                                     
''')

# Start an instance of pyjapc and choose the correct USER
japc = pyjapc.PyJapc(noSet=False)
japc.rbacLogin()
user = "CPS.USER.SFTPRO3"
East_user = "CPS.USER.SFTPRO3"
japc.setSelector(user)

# BCT in the PSB to skip low intenstiy shots
bct = "BTP.BCT10/Acquisition#totalIntensityCalcCh1"
bct_threshold = 5
PSB_user = "PSB.USER.MD8"

# Create a folder to save data
root_folder, folder_name, time_title = fs.create_folder("quad_scan_east_dump_data", "quad_scan_east_dump")

# Data we want to save
timestamp_list = []

btv_name_list = ["F61D.BTV010"]
btv_dict = {btv_name + "/Acquisition": [] for btv_name in btv_name_list}
btv_image_dict = {btv_name + "/Image": [] for btv_name in btv_name_list}

converter_name_list = ["F61.QFN01/MEAS.PULSE#VALUE",
                       "F61.QDN02/MEAS.PULSE#VALUE",
                       "F61.QFN03/MEAS.PULSE#VALUE"]
converter_dict = {converter_name: [] for converter_name in converter_name_list}

sigma_dict = {"sig_h0": [], "sig_h1": [], "sig_h2": [], "sig_h3": [], "sig_h4": [], "sig_h5": [], "sig_h6": [],
              "sig_v0": [], "sig_v1": [], "sig_v2": [], "sig_v3": [], "sig_v4": [], "sig_v5": [], "sig_v6": []}
sigma_err_dict = {"sig_h0": [], "sig_h1": [], "sig_h2": [], "sig_h3": [], "sig_h4": [], "sig_h5": [], "sig_h6": [],
                  "sig_v0": [], "sig_v1": [], "sig_v2": [], "sig_v3": [], "sig_v4": [], "sig_v5": [], "sig_v6": []}

# # Initial CONVERTERS currents
print(80 * "_")
print("Initial Converters settings:\n")
japc.setParam("F61.QFN01/REF.PULSE.AMPLITUDE#VALUE", 100)
japc.setParam("F61.QDN02/REF.PULSE.AMPLITUDE#VALUE", 382)
japc.setParam("F61.QFN03/REF.PULSE.AMPLITUDE#VALUE", 387)
print(80 * "_" + "\n")

# Initial BTV parameters
print(80 * "_")
print("Initial BTV settings:\n")
print("Selecting all images to acquire")
japc.setParam(btv_name_list[0] + "/MultiplexedSetting#reqUserImageSelection",
              [True, True, True, True, True, True, True])
japc.setSelector("")  # Following settings requires no selector
print("Turning on Camera Switch")
japc.setParam(btv_name_list[0] + "/Setting#screenSelect", 'FIRST')
print("Inserting Screen")
japc.setParam(btv_name_list[0] + "/Setting#cameraSwitch", 'CCD_ON_RAD_STDBY')
japc.setSelector(user)
print(80 * "_" + "\n")

print("Starting SCAN\n")
iteration_number = 0


Brho = 24*3.3356
quad_length = {
    "QFN01": 0.74,
    "QDN02": 1.2,
    "QFN03": 1.2,
}
# QFN01 -733 to 733 [A], type Q74L
# QDN02 -537 to 537 [A], type Q120C
# QFN03 -421 to 421 [A], type QFL


quadrupole_to_scan = "F61.QFN01/REF.PULSE.AMPLITUDE#VALUE"

# Max currents: Q74L is +-733, Q120C is +-537, QFL is +-421
current1 = np.linspace(500, 400, 20)
current2 = np.linspace(320, 480, 20)
current3 = np.linspace(290, 420, 20)
x_axis = []

fig, ax = plt.subplots(2, 2, figsize=(10, 8), tight_layout=True)

ax_V_sigma = ax[1,1].twinx()
fig.canvas.manager.set_window_title(f'Quadrupole Scan {time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss")}')
fig.suptitle(f'Quadrupole scan East Dump {time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss")}')
converter_dict_animation = {converter_name: [] for converter_name in converter_name_list}

# Initial image on the second window
btv_data_image = japc.getParam(btv_name_list[0] + "/Image", timingSelectorOverride=East_user)
number_of_acqu = len(btv_data_image["imageSet"])

##################
# Callback function, gets called everytime the value we're subscribed to gets updated
##################
def myCallback(parameterName, newValue, header):
    # Check if BCT in the PSB is above threshold
    bct_intensity = japc.getParam(bct, timingSelectorOverride=PSB_user)
    print (f"bct intensity = {bct_intensity}")
    if bct_intensity > bct_threshold:
        ax[0, 0].clear()
        ax[1, 0].clear()
        time = datetime.datetime.now()
        print(f"New Shot for {parameterName} at {time.hour}h{time.minute}:{time.second}")
        global iteration_number
        print(f"Iteration number: {iteration_number}")
        x_axis.append(iteration_number)
        # Append timestamp
        timestamp_list.append(time)

        # Set new K1 to converter we scan
        # print(f"Setting {logical_k_to_scan} k to: {round(k_to_scan[iteration_number],3)}")
        #
        # my_array = [np.array([[0, 429], [k_to_scan[iteration_number], k_to_scan[iteration_number]]])]
        #
        # japc.setParam(logical_k_to_scan, my_array)
        # k_list.append(k_to_scan[iteration_number])
        # k_dict["k1"].append(my_array)

        # ax[0, 1].plot(my_array[0][0], my_array[0][1], marker="o", label=f"k1 = {round(k_to_scan[iteration_number], 3)}")
        # ax[0, 1].set_xlabel("Time [ms]")
        # ax[0, 1].set_ylabel("K1")
        # ax[0, 1].set_title("Logical K")

        for converter_name in converter_name_list:
            current = japc.getParam(converter_name)
            converter_dict_animation[converter_name].append(current)
            ax[0, 0].plot(timestamp_list, converter_dict_animation[converter_name], marker="o", label=converter_name)
            ax[0, 0].set_xlabel("Local Time")
            ax[0, 0].set_ylabel("Current [A]")
            ax[0, 0].set_title("Converters")
            ax[0, 0].legend()
            ax[0, 0].fmt_xdata = mdates.DateFormatter('%Y-%m-%d')  # Prettier date time
            ax[0, 0].tick_params('x', labelrotation=45)

        # Append all new btv data (projections, etc) for the new shot
        for btv_name in btv_name_list:
            btv_data = japc.getParam(btv_name + "/Acquisition")
            btv_dict[btv_name + "/Acquisition"].append(btv_data)

            H_mu_list = []
            H_mu_err_list = []
            V_mu_list = []
            V_mu_err_list = []
            H_sigma_list = []
            H_sigma_err_list = []
            V_sigma_list = []
            V_sigma_err_list = []

            for i in range(len(btv_dict[btv_name + "/Acquisition"][-1]["positionSet1"])):
                # Horizontal Gaussian Fit
                Hx = btv_dict[btv_name + "/Acquisition"][-1]["projPositionSet1"][i]
                Hy = btv_dict[btv_name + "/Acquisition"][-1]["projDataSet1"][i]

                try:
                    popt, pcov = fs.do_gaussian_fit(Hx, Hy)
                    H_mu_list.append(popt[2])
                    H_mu_err_list.append(pcov[2, 2] ** 0.5)
                    sigma = abs(popt[3])
                    H_sigma_list.append(sigma)
                    sigma_err = pcov[3, 3] ** 0.5
                    H_sigma_err_list.append(sigma_err)

                    sigma_dict["sig_h"+str(i)].append(sigma)
                    sigma_err_dict["sig_h" + str(i)].append(sigma_err)

                except:
                    # print ("failed to do the gaussian fit")
                    sigma_dict["sig_h" + str(i)].append(np.nan)
                    sigma_err_dict["sig_h" + str(i)].append(np.nan)
                    pass
                # Vertical Gaussian Fit
                Vx = btv_dict[btv_name + "/Acquisition"][-1]["projPositionSet2"][i]
                Vy = btv_dict[btv_name + "/Acquisition"][-1]["projDataSet2"][i]

                try:
                    popt, pcov = fs.do_gaussian_fit(Vx, Vy)
                    V_mu_list.append(popt[2])
                    V_mu_err_list.append(pcov[2, 2] ** 0.5)
                    sigma = abs(popt[3])
                    V_sigma_list.append(sigma)
                    sigma_err = pcov[3, 3] ** 0.5
                    V_sigma_err_list.append(sigma_err)

                    sigma_dict["sig_v" + str(i)].append(sigma)
                    sigma_err_dict["sig_v" + str(i)].append(sigma_err)


                except:
                    # print ("failed to do the gaussian fit")
                    sigma_dict["sig_v" + str(i)].append(np.nan)
                    sigma_err_dict["sig_v" + str(i)].append(np.nan)
                    pass

            color = iter(cm.plasma(np.linspace(1, 0, 8)))
            for key_sigma in ["sig_h3"]:
                c = next(color)
                ax[1, 1].errorbar(x_axis, np.array(sigma_dict[key_sigma])/1000,
                                    yerr=np.array(sigma_err_dict[key_sigma])/1000,
                                    xerr=None,
                                    fmt = "o",
                                    linestyle='dotted',
                                    color = 'b',
                                    markeredgecolor='b',
                                    ecolor="b",
                                    label="$\sigma_{H}$")

            color = iter(cm.plasma(np.linspace(1, 0, 8)))
            for key_sigma in ["sig_v3"]:
                c = next(color)
                ax_V_sigma.errorbar(x_axis, np.array(sigma_dict[key_sigma])/1000,
                                    yerr=np.array(sigma_err_dict[key_sigma])/1000,
                                    xerr=None,
                                    fmt = "o",
                                    linestyle='dotted',
                                    color = 'r',
                                    markeredgecolor='r',
                                    ecolor="r",
                                    label="$\sigma_{V}$")

            ax[1, 1].set_title(f"Beam size")
            ax[1, 1].set_xlabel("Iteration number")
            ax[1, 1].set_ylabel("H beam size 1$\sigma$ [m]", color="b")
            ax[1, 1].tick_params(axis='y', labelcolor="b")
            ax_V_sigma.set_ylabel("V beam size 1$\sigma$ [m]", color="r")
            ax_V_sigma.tick_params(axis='y', labelcolor="r")


        # Append btv image to the new shot
        global btv_data_image
        global number_of_acqu
        btv_data_image = japc.getParam(btv_name_list[0] + "/Image", timingSelectorOverride=East_user)
        number_of_acqu = len(btv_data_image["imageSet"])

        btv_image_dict[btv_name + "/Image"].append(btv_data_image)
        for acq_number in range(number_of_acqu):
            pixel_y = len(btv_data_image["imagePositionSet2"][0])
            try:
                image = btv_data_image["imageSet"][acq_number]
                reshaped_image = image.reshape(pixel_y, -1)
            except:
                image = btv_data_image["imageSet"][0]  # If there are not enough frame, just send first frame
                reshaped_image = image.reshape(pixel_y, -1)
            ax[1, 0].imshow(reshaped_image, interpolation='nearest', cmap="jet", alpha=1 / number_of_acqu)
        ax[1, 0].set_title(f"Acq")
        ax[1, 0].set_xlabel("x [?]")
        ax[1, 0].set_ylabel("Y [?]")
        ax[1, 0].set_title(f"Average of {number_of_acqu} acqu.")

        # Append all new currents for the new shot
        for converter_name in converter_name_list:
            current = japc.getParam(converter_name)
            converter_dict[converter_name].append(current)

        #Update the time of last shot
        global time_last_shot
        time_last_shot = japc.getParam(btv_name_list[0] + "/Acquisition#acqTime", timingSelectorOverride=East_user)

        try:
            # JSON data dump
            with open(root_folder + "/" + folder_name + "/" + folder_name + ".json", 'w') as outfile:
                outfile.write(json.dumps((timestamp_list, btv_dict, btv_image_dict, converter_dict), cls=myJSONEncoder))

            # Pickle data dump
            pickle.dump((timestamp_list, btv_dict, btv_image_dict, converter_dict),
                        open(root_folder + "/" + folder_name + "/" + folder_name + '.p', 'wb'))
        except:
            print('Error on saving data json pickle')

        # Increment the current to scan
        if iteration_number >= (len(current1) - 1):
            print(f"SCAN COMPLETED")
            iteration_number = (len(current1) - 2)
        # japc.stopSubscriptions()
        # japc.clearSubscriptions()
        # sys.exit("Scan completed")
        iteration_number += 1

        # Update the plot
        fig.canvas.draw()

        # Change the current in the QUAD
        print(f"Setting F61.QFN01/REF.PULSE.AMPLITUDE#VALUE to: {round(current1[iteration_number], 3)}")
        print(f"Setting F61.QDN02/REF.PULSE.AMPLITUDE#VALUE to: {round(current2[iteration_number], 3)}")
        print(f"Setting F61.QFN03/REF.PULSE.AMPLITUDE#VALUE to: {round(current3[iteration_number], 3)}")
        japc.setParam("F61.QFN01/REF.PULSE.AMPLITUDE#VALUE", current1[iteration_number])
        japc.setParam("F61.QDN02/REF.PULSE.AMPLITUDE#VALUE", current2[iteration_number])
        japc.setParam("F61.QFN03/REF.PULSE.AMPLITUDE#VALUE", current3[iteration_number])

        try:
            # Save figure
            fig.savefig(root_folder + "/" + folder_name + "/" + folder_name + ".png", facecolor='white', transparent=False,
                        bbox_inches='tight');
        except:
            print('error on saving image')
    else:
        print ("Skipping shot due to low intensity on BCT")
# Everytime this value changes we run the callback function
japc.subscribeParam("F61D.BTV010/Acquisition", myCallback, getHeader=True)
japc.startSubscriptions()
fig.show()






# Create the window for the animation of the last BTV shot
fig2, ax2 = plt.subplots(tight_layout=True)
time_last_shot = japc.getParam(btv_name_list[0] + "/Acquisition#acqTime", timingSelectorOverride=East_user)
fig2.canvas.manager.set_window_title(f'Last Shot {time_last_shot}')
fig2.show()

btv_data_image = japc.getParam(btv_name_list[0] + "/Image")
pixel_y = len(btv_data_image["imagePositionSet2"][0])
image = btv_data_image["imageSet"][1]
reshaped_image = image.reshape(pixel_y, -1)
im = ax2.imshow(reshaped_image, animated=True, cmap="jet")

acq_number = 0
def animate_func(*args):

    fig2.canvas.manager.set_window_title(f'Last Shot {time_last_shot}')

    global acq_number
    pixel_y = len(btv_data_image["imagePositionSet2"][0])
    try:  # Handling exception if we don't have 7 shots
        image = btv_data_image["imageSet"][acq_number]
        reshaped_image = image.reshape(pixel_y, -1)
        im.set_array(reshaped_image)
    except:
        image = btv_data_image["imageSet"][0]  # If there are not enough frame, just send first frame
        reshaped_image = image.reshape(pixel_y, -1)
    # ax2.set_title(f"Acq: {i}")
    acq_number += 1
    if acq_number == 6:
        acq_number = 0

    return im,

anim = animation.FuncAnimation(
    fig2,
    animate_func,
    blit = True,
    interval=100) # in ms
