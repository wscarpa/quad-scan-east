from cpymad.madx import Madx
import numpy as np
import matplotlib.pyplot as plt
import sys
sys.path.append("/afs/cern.ch/work/e/eljohnso/public/madx-tools")
import plot_tool
import importlib
importlib.reload(plot_tool)
from plot_tool import *
import pyjapc
import datetime
japc = pyjapc.PyJapc(noSet=True)
japc.setSelector("CPS.USER.EAST3")

# Beam characteristics
gamma = 25.598474067
beta = np.sqrt(1-gamma**(-2))
exn = 4.92e-06
eyn = 3.4e-06
sige = 0.000412
ex = exn/(beta*gamma)
ey = eyn/(beta*gamma)
Brho = 24*3.3356

# Matched initial parameters
betx0 = 154.0835045206266
bety0 = 5.222566527078791
alfx0 = -36.90472944993891
alfy0 = 0.2523074897915478
Dx0 = 0.13
Dy0 = 0.0
Dpx0 = 0.02
Dpy0 = 0.0
exn = 7.639770207283603e-06
eyn =  3.534081877201574e-06
sige = 0.000679081344780741

with open('tempfile', 'w') as f:
    madx = Madx(stdout=f,stderr=f)
    madx.option(verbose=True, debug=False, echo=True, warn=True, twiss_print=False)

madx.call("f61t8_op.str")
madx.call("f61t8_op.seq")

ex = exn/(beta*gamma)
ey = eyn/(beta*gamma)

madx.command.beam(particle='PROTON',pc="24",ex=ex,ey=ey)
madx.input('BRHO      := BEAM->PC * 3.3356;')


madx.input('SELECT, FLAG=TWISS, COLUMN=NAME,KEYWORD,S,L,K1L,BETX,ALFX,X,DX,PX,DPX,MUX,BETY,ALFY,Y,DY,PY,DPY,MUY,APER_1,APER_2,APER_3,APER_4,APERTYPE,KMIN,RE11,RE12,RE21,RE22,RE33,RE34,RE43,RE44,RE16,RE26;')

madx.input('SEQEDIT, SEQUENCE=f61t8_op;')
madx.input('FLATTEN;')
madx.input('REMOVE, ELEMENT=ARBMATRIX;')
madx.input('FLATTEN;')
madx.input('ENDEDIT;')

madx.input("F62.BTV002 : MARKER;")
madx.input('SEQEDIT, sequence=f61t8_op;')
madx.input('FLATTEN;')
madx.command.install(element = 'F62.BTV002', at=1.250107+0.406, from_="F61.MBXHD033")
madx.input('FLATTEN;')
madx.input('ENDEDIT;')

madx.use(sequence="f61t8_op")

fig, ax = plt.subplots(3,1, figsize=(10,7), tight_layout=True, sharex=True, height_ratios=[1,3,3])

time_at_script_start = datetime.datetime.now()
fig.canvas.manager.set_window_title(
    f'Quadrupole Scan started at: {time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss")}')
fig.suptitle(f'Quadrupole scan East Dump {time_at_script_start.strftime("%Y_%m_%d_%Hh%Mm%Ss")}')

def plot_data(ax, x, y, color="k", marker="x"):
    ax.plot(x, y, marker=marker, color=color)
    ax.plot(x, -y, marker=marker, color=color)

fontsize = 16

def myCallback(parameterName, newValue):
    ax[0].clear()
    ax[1].clear()
    ax[2].clear()
    time = datetime.datetime.now()
    fig.suptitle(time.strftime("%Y_%m_%d_%Hh%Mm%Ss"))

    # Grab Data of Instruments
    BPM1 = japc.getParam("PS-LOG-BPM-IRRAD-UCAP_BPM_01/Positions")
    BPM2 = japc.getParam("PS-LOG-BPM-IRRAD-UCAP_BPM_02/Positions")
    BPM3 = japc.getParam("PS-LOG-BPM-IRRAD-UCAP_BPM_03/Positions")
    BPM4 = japc.getParam("PS-LOG-BPM-IRRAD-UCAP_BPM_04/Positions")
    MWPC = japc.getParam("PS-LOG-MWPC-UCAP_IRRAD_2080/LowGainPositions")

    # Grab Data of Quadrupole
    k_quadrupole_list = [
        japc.getParam("logical.F61.QFN01/K_FUNC_LIST")[0][1][0],
        japc.getParam("logical.F61.QDN02/K_FUNC_LIST")[0][1][0],
        japc.getParam("logical.F61.QFN03/K_FUNC_LIST")[0][1][0],
        japc.getParam("logical.F61.QDN04/K_FUNC_LIST")[0][1][0],
        japc.getParam("logical.T8.QFN05/K_FUNC_LIST")[0][1][0],
        japc.getParam("logical.T8.QDN06/K_FUNC_LIST")[0][1][0],
        japc.getParam("logical.T8.QDN07/K_FUNC_LIST")[0][1][0],
        japc.getParam("logical.T8.QFN08/K_FUNC_LIST")[0][1][0],
    ]

    madx.input("kQFN1 = " + str(k_quadrupole_list[0]) + ";")
    madx.input("kQDN2 = " + str(k_quadrupole_list[1]) + ";")
    madx.input("kQFN3 = " + str(k_quadrupole_list[2]) + ";")
    madx.input("kQDN4 = " + str(k_quadrupole_list[3]) + ";")
    madx.input("kQFN5 = " + str(k_quadrupole_list[4]) + ";")
    madx.input("kQDN6 = " + str(k_quadrupole_list[5]) + ";")
    madx.input("kQDN7 = " + str(k_quadrupole_list[6]) + ";")
    madx.input("kQFN8 = " + str(k_quadrupole_list[7]) + ";")
    twiss_f61 = madx.twiss(betx=betx0, bety=bety0, alfx=alfx0, alfy=alfy0, Dx=Dx0, Dy=Dy0, Dpx=Dpx0, Dpy=Dpy0,
                           file="twiss_f61.tfs").dframe()


    draw_synoptic(ax[0], twiss_f61)
    ax[0].set_xlim(0, twiss_f61.s[-1])
    ax[0].set_ylim(-0.02, 1.)
    ax[0].axis("off")

    ax[1].plot(twiss_f61['s'], beam_size(twiss_f61['betx'], twiss_f61['dx'], ex, sige, 1), alpha=1.0, color="b",
               zorder=0)
    ax[1].plot(twiss_f61['s'], -beam_size(twiss_f61['betx'], twiss_f61['dx'], ex, sige, 1), alpha=1.0, color="b",
               zorder=0)
    draw_aperture_circle(ax[1], twiss_f61, "aper_1")
    draw_aperture_rectangle(ax[1], twiss_f61, "aper_1")
    # IRRAD BPM
    plot_data(ax[1], twiss_f61.loc["t08.bpm073"].s, BPM1["HSigma"] / 1000)
    plot_data(ax[1], twiss_f61.loc["t08.bpm080"].s, BPM2["HSigma"] / 1000)
    plot_data(ax[1], twiss_f61.loc["t08.bpm085"].s, BPM3["HSigma"] / 1000)
    plot_data(ax[1], twiss_f61.loc["t08.bpm092"].s, BPM4["HSigma"] / 1000)
    plot_data(ax[1], twiss_f61.loc["t08.xwcm103"].s, MWPC["HSigma"] / 1000)
    ax[1].set_xlim(0, twiss_f61.s[-1])

    ax[2].plot(twiss_f61['s'], beam_size(twiss_f61['bety'], twiss_f61['dy'], ey, sige, 1) + twiss_f61.y, alpha=1.0,
               color="r", zorder=0)
    ax[2].plot(twiss_f61['s'], -beam_size(twiss_f61['bety'], twiss_f61['dy'], ey, sige, 1) + twiss_f61.y, alpha=1.0,
               color="r", zorder=0)
    draw_aperture_circle(ax[2], twiss_f61, "aper_1")
    draw_aperture_rectangle(ax[2], twiss_f61, "aper_2")
    plot_data(ax[2], twiss_f61.loc["t08.bpm073"].s, BPM1["VSigma"] / 1000)
    plot_data(ax[2], twiss_f61.loc["t08.bpm080"].s, BPM2["VSigma"] / 1000)
    plot_data(ax[2], twiss_f61.loc["t08.bpm085"].s, BPM3["VSigma"] / 1000)
    plot_data(ax[2], twiss_f61.loc["t08.bpm092"].s, BPM4["VSigma"] / 1000)
    plot_data(ax[2], twiss_f61.loc["t08.xwcm103"].s, MWPC["VSigma"] / 1000)
    ax[2].set_xlim(0, twiss_f61.s[-1])

    # IRRAD target size
    ax[1].axhline(13/1000/2.355, ls="--") # To get the sigma we divide by 2.355 because he gave us the FWHM
    ax[1].axhline(-13 / 1000 / 2.355, ls="--")
    ax[2].axhline(13.6/1000/2.355, ls="--")
    ax[2].axhline(-13.6 / 1000 / 2.355, ls="--")

    ax[1].grid()
    ax[1].set_ylim(-0.12, 0.12)
    ax[1].set_ylabel(r"$\sigma_{H}$ [m]", fontsize=fontsize)

    ax[2].grid()
    ax[2].set_ylim(-0.12, 0.12)
    ax[2].set_ylabel(r"$\sigma_{V}$ [m]", fontsize=fontsize)

    # Add a text box with the fitted_params_parallel_12_mm values
    params_text = "Quadrupole strength:\n"
    params_text += "kQFN1 = {}\n".format(k_quadrupole_list[0])
    params_text += "kQDN2 = {}\n".format(k_quadrupole_list[1])
    params_text += "kQFN3 = {}\n".format(k_quadrupole_list[2])
    params_text += "kQDN4 = {}\n".format(k_quadrupole_list[3])
    params_text += "kQFN5 = {}\n".format(k_quadrupole_list[4])
    params_text += "kQDN6 = {}\n".format(k_quadrupole_list[5])
    params_text += "kQDN7 = {}\n".format(k_quadrupole_list[6])
    params_text += "kQFN8 = {}\n".format(k_quadrupole_list[7])
    ax[1].text(0, -0.12, params_text, fontsize=10, bbox={'facecolor': 'white', 'alpha': 1.0, 'pad': 2})

    # Adjust the spacing between the two subplots
    plt.subplots_adjust(wspace=1)


    fig.canvas.draw()


japc.subscribeParam("F61.QFN01/MEAS.PULSE#VALUE", myCallback)
japc.startSubscriptions()
fig.show()