import numpy as np
import pyjapc
import matplotlib.pyplot as plt
import pandas as pd
import datetime
import functions as fs
import pickle

time_at_script_start = datetime.datetime.now()

# Create a folder to save data
root_folder, folder_name, time_title = fs.create_folder("spill_data_bxscint",
                                                        "spill_data_bxscint")

japc = pyjapc.PyJapc(noSet=True)
japc.setSelector("CPS.USER.EAST3")

fig, ax = plt.subplots(2,1, tight_layout=True)
fig.canvas.manager.set_window_title(f'Spill monitor started at: {time_at_script_start.strftime("%Y %m %d %Hh%Mm%Ss")}')
fig.suptitle(f'Spill monitor {time_at_script_start.strftime("%Y %m %d %Hh%Mm%Ss")}')

# First data for the heatmap
signal_1000 = japc.getParam("BXSCINT_1000/Acquisition#countArray")
fifoFreq = japc.getParam("BXSCINT_1000/Acquisition#fifoFreq")

nbOfSamples = len(signal_1000)
integrationDuration = nbOfSamples*(1/fifoFreq)*1000 # in ms

heatmap = np.reshape(signal_1000, (1, nbOfSamples))
time_list = [time_at_script_start.strftime("%Hh%Mm%Ss")]

# Data we want to save
timestamp_list = []
spill_monitor_name_list = ["BXSCINT_1000/Acquisition",
                       "BXSCINT_1001/Acquisition",]
spill_monitor_dict = {spill_monitor: [] for spill_monitor in spill_monitor_name_list}

def myCallback(parameterName, newValue):

    global heatmap
    global time_list

    ax[0].clear()
    ax[1].clear()
    time = datetime.datetime.now()

    time_list.append(time.strftime("%Hh%Mm%Ss"))
    fig.suptitle(time.strftime("Spill waterfall East Area \n Most recent acquisition: %Hh%Mm%Ss"))

    t = np.arange(0, integrationDuration, integrationDuration / nbOfSamples)# time in ms

    # Raw data
    signal_1000 = japc.getParam("BXSCINT_1000/Acquisition#countArray")
    signal_1001 = japc.getParam("BXSCINT_1001/Acquisition#countArray")


    # Append all new data for the new shot
    timestamp_list.append(time)
    for spill_monitor in spill_monitor_name_list:
        data = japc.getParam(spill_monitor)
        spill_monitor_dict[spill_monitor].append(data)

    d = {'t': t, 'signal_1000': signal_1000, 'signal_1001': signal_1001}
    df = pd.DataFrame(data=d)

    ax[0].plot(df.t, df.signal_1000, color="b", alpha=0.3, label="Raw BXSCINT 1000")
    ax[0].plot(df.t, df.signal_1001, color="r", alpha=0.3, label="Raw BXSCINT 1001")

    # Rolling average
    window = 20
    df2 = df.rolling(window=window).mean()
    ax[0].plot(df.t, df2.signal_1000, color="b", alpha=1., label=f"rolling mean window = {window}")
    ax[0].plot(df.t, df2.signal_1001, color="r", alpha=1., label=f"rolling mean window = {window}")

    # Waterfall heatmap
    try:
        heatmap=np.vstack([df2.signal_1000, heatmap])
    except:
        print("error stacking new data into matrix")

    ax[1].imshow(heatmap, cmap="magma", aspect='auto', interpolation="none")

    ax[0].set_xlabel("Time [ms]")
    ax[0].set_ylabel("Amplitude [arb.]")
    ax[0].set_xlim(0,t.max())
    ax[0].legend(loc="upper left")

    x_axis = ax[1].axes.get_xaxis()
    x_axis.set_visible(False)
    step = np.shape(heatmap)[0]//10 + 1
    ax[1].set_yticks(np.arange(0, np.shape(heatmap)[0],  step))
    ax[1].set_yticklabels(time_list[::-step]) # Reverse order

    ax[1].set_ylabel("Time")

    fig.canvas.draw() # Update the plot

    try:
        # Pickle data dump
        pickle.dump((timestamp_list, spill_monitor_dict), open(root_folder + "/" + folder_name + "/" + folder_name + '.p', 'wb'))
    except:
        print('Error on saving data json pickle')
    try:
        # Save figure
        fig.savefig(root_folder + "/" + folder_name + "/" + folder_name + ".png", facecolor='white',
                    transparent=False,
                    bbox_inches='tight');
    except:
        print('error on saving image')

japc.subscribeParam("BXSCINT_1000/Acquisition#countArray", myCallback)
japc.startSubscriptions()
fig.show()
